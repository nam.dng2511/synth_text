# Adding New Images
Segmentation and depth-maps are required to use new images as background. Sample scripts for obtaining these are available [here](https://github.com/ankush-me/SynthText/tree/master/prep_scripts). 

(**Warning: This task require MATLAB on LINUX**)

* Prepare `matconvnet` ([Follow instructions](http://www.vlfeat.org/matconvnet/install/)) and `vlfeat-0.9.18` for MATLAB and put it into some folder. For example:
```
prep_scripts/libs/matconvnet
prep_scripts/libs/vlfeat-0.9.18
```

* [Download](https://drive.google.com/drive/folders/16gHFkudN-VKJcu4Z9qXsiczWIVWerf12?usp=sharing) pretrained model for predict depth task.

* `predict_depth.m` MATLAB script to regress a depth mask for a given RGB image; uses the network of [Liu etal.](https://bitbucket.org/fayao/dcnf-fcsp/). However, more recent works (e.g., [this](https://github.com/iro-cp/FCRN-DepthPrediction)) might give better results. 

* `run_ucm.m` and `floodFill.py` for getting segmentation masks using [gPb-UCM](https://github.com/jponttuset/mcg) or [COB](https://github.com/kmaninis/COB).

**Note: You might have to config some path:**

`predict_depth.m`
```
dir_vlfeat = 'path/to/vlfeat'
dir_matConvNet = 'path/to/matconvnet/'
opts.imdir = 'path/to/input_image'
opts.out_h5 = 'path/to/output_result.h5'
opts.model_file.indoor = '/path/to/model_dcnf-fcsp_NYUD2'
opts.model_file.outdoor = 'path/to/model_dcnf-fcsp_Make3D'
```
After processing, `predict_depth.m` return a .h5 contain "predict depth images".

`run_ucm.m`
```
img_dir = '..path/to/new_images';
mcg_dir = '.path/to/mcg/pre-trained';
```
After processing, `run_ucm.m` return `ucm.mat` contrain masks. 

Run `floodFill.py` with following argument:
```
python floodFill.py --ucm <path to ucm.mat> --output <output path>
```
For more detail:
```
python floodFill.py --help
```
For an explanation of the fields in `dset.h5` (e.g.: `seg`,`area`,`label`), please check this [comment](https://github.com/ankush-me/SynthText/issues/5#issuecomment-274490044).

run `create_im_name.py` with corresponding image path. This will return `imnames.cp`