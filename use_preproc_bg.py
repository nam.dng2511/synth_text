"""
Sample code for load the 8000 pre-processed background image data.
Before running, first download the files from:
  https://github.com/ankush-me/SynthText#pre-generated-dataset
"""

import h5py
import numpy as np
from PIL import Image
import os.path as osp
import pickle as cp
import os

##### write to h5 file #####
DATABASE_FOLDER = 'database'
IMAGE_FOLDER = 'prep_scripts'
dataset = h5py.File(os.path.join(DATABASE_FOLDER, "test.h5"), "w")
# create groups
image_group = dataset.create_group("image")
depth_group = dataset.create_group("depth")
seg_group = dataset.create_group("seg")

im_dir = os.path.join(IMAGE_FOLDER, "new_images")
depth_db = h5py.File(os.path.join(DATABASE_FOLDER, "depth.h5"),'r')
seg_db = h5py.File(os.path.join(DATABASE_FOLDER, "seg.h5"),'r')

imnames = sorted(depth_db.keys())
with open(os.path.join(IMAGE_FOLDER, "imnames.cp"), 'rb') as f:
  filtered_imnames = set(cp.load(f))

for index, imname in enumerate(os.listdir(im_dir)):
  # use specific background
  # if imname != 'ant+hill_114.jpg':
  #   continue

  # start running from crash
  # if index <= 6010:
  #   continue

  print('Processing {}/{} image {}'.format(index, len(imnames), imname))
  # ignore if not in filetered list:s
  if imname not in filtered_imnames: continue
  if imname in ['ant+hill_130.jpg', 'coffee_87.jpg', 'fruits_130.jpg', 'fruits_23.jpg', 'hubble_44.jpg', 'jogging_60.jpg', 'kerala_37.jpg', 'kerala_90.jpg', 'planet_109.jpg', 'rajasthan_109.jpg', 'san+diego_11.jpg']:
    continue

  # if imname not in ['cambridge_148.jpg', 'camping_94.jpg', 'cows_82.jpg', 'crater_97.jpg', 'farmer_17.jpg', 'farmer_44.jpg', 'jaipur_131.jpg', 'park_46.jpg', 'peacock_4.jpg', 'planet_89.jpg']:
  #   continue
  
  # get the colour image:
  img = Image.open(osp.join(im_dir, imname)).convert('RGB')
  
  # get depth:
  depth = depth_db[imname][:].T
  depth = depth[:,:,0]

  # get segmentation info:

  seg = seg_db['mask'][imname][:].astype('float32')
  area = seg_db['mask'][imname].attrs['area']
  label = seg_db['mask'][imname].attrs['label']

  # re-size uniformly:
  sz = depth.shape[:2][::-1]
  img = np.array(img.resize(sz,Image.ANTIALIAS))
  seg = np.array(Image.fromarray(seg).resize(sz,Image.NEAREST))

  # write image
  dataset['image'].create_dataset(imname,data=img)

  # write depth
  dataset['depth'].create_dataset(imname,data=depth_db[imname][:])

  # write seg
  dataset['seg'].create_dataset(imname,data=seg_db['mask'][imname][:])
  dataset['seg'][imname].attrs['area'] = seg_db['mask'][imname].attrs['area']
  dataset['seg'][imname].attrs['label'] = seg_db['mask'][imname].attrs['label']

  # # check img
  # print('check img: ', np.array(Image.fromarray(dataset['image'][imname][:])).shape)

  # # check depth
  # print('check depth: ', depth_db[imname][:].shape)

  # if index >= 0:
  #   break

  # see `gen.py` for how to use img, depth, seg, area, label for further processing.
  #    https://github.com/ankush-me/SynthText/blob/master/gen.py
