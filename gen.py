# Author: Ankush Gupta
# Date: 2015

"""
Entry-point for generating synthetic text images, as described in:

@InProceedings{Gupta16,
      author       = "Gupta, A. and Vedaldi, A. and Zisserman, A.",
      title        = "Synthetic Data for Text Localisation in Natural Images",
      booktitle    = "IEEE Conference on Computer Vision and Pattern Recognition",
      year         = "2016",
    }
"""

import numpy as np
import h5py
import os, sys, traceback
import os.path as osp
from synthgen import *
from common import *
import wget, tarfile
from scipy.io import savemat
import cv2
import time


## Define some configuration variables:
NUM_IMG = -1 # no. of images to use for generation (-1 to use all available):
INSTANCE_PER_IMAGE = 1 # no. of times to use the same image
SECS_PER_IMG = None #max time per image in seconds

# path to the data-file, containing image, depth and segmentation:
DATA_PATH = 'data'
# DB_FNAME = osp.join(DATA_PATH,'dset.h5')
DB_FNAME = os.getcwd() + '/database/' + 'test.h5'
# url of the data (google-drive public file):
DATA_URL = 'http://www.robots.ox.ac.uk/~ankush/data.tar.gz'
OUT_FILE = 'results/SynthText.h5'
OUT_DIR = 'data_generated/sakai_07_02'
SAVE_MAT_INTERVAL = 500

def get_data():
	"""
	Download the image,depth and segmentation data:
	Returns, the h5 database.
	"""
	if not osp.exists(DB_FNAME):
		try:
			colorprint(Color.BLUE,'\tdownloading data (56 M) from: '+DATA_URL,bold=True)
			print()
			sys.stdout.flush()
			out_fname = 'data.tar.gz'
			wget.download(DATA_URL,out=out_fname)
			tar = tarfile.open(out_fname)
			tar.extractall()
			tar.close()
			os.remove(out_fname)
			colorprint(Color.BLUE,'\n\tdata saved at:'+DB_FNAME,bold=True)
			sys.stdout.flush()
		except:
			print (colorize(Color.RED,'Data not found and have problems downloading.',bold=True))
			sys.stdout.flush()
			sys.exit(-1)
	# open the h5 file and return:
	return h5py.File(DB_FNAME,'r')


def add_res_to_db(imgname,res,db):
	"""
	Add the synthetically generated text image instance
	and other metadata to the dataset.
	"""
	ninstance = len(res)
	for i in range(ninstance):
		dname = "%s_%d"%(imgname, i)
		db['data'].create_dataset(dname,data=res[i]['img'])
		db['data'][dname].attrs['charBB'] = res[i]['charBB']
		db['data'][dname].attrs['wordBB'] = res[i]['wordBB']        
		text_utf8 = [char.encode('utf8') for char in res[i]['txt']]
		db['data'][dname].attrs['txt'] = text_utf8
		L = res[i]['txt']
		L = [n.encode("ascii", "ignore") for n in L]
		db['data'][dname].attrs['txt'] = L


def save_res_to_imgs(imgname, res):
	"""
	Add the synthetically generated text image instance
	and other metadata to the dataset.
	"""
	ninstance = len(res)
	for i in range(ninstance):
		filename = "{}/{}_{}.png".format(OUT_DIR, imgname, i)
		# Swap bgr to rgb so we can save into image file
		img = res[i]['img'][..., [2, 1, 0]]
		cv2.imwrite(filename, img)

def save_file_mat(dict_dot_mat, image_count):
	#truncate
	dict_dot_mat['imnames'] = np.resize(dict_dot_mat['imnames'], (1, image_count))
	dict_dot_mat['wordBB'] = np.resize(dict_dot_mat['wordBB'], (1, image_count))
	dict_dot_mat['charBB'] = np.resize(dict_dot_mat['charBB'], (1, image_count))
	dict_dot_mat['txt'] = np.resize(dict_dot_mat['txt'], (1, image_count))
	print(dict_dot_mat['imnames'])
	print('image_count: ', image_count)
	savemat(os.getcwd() + '/' + OUT_DIR + '/synth/bg.mat', dict_dot_mat)


def main(viz=False):
	start_time = time.time()
	# open databases:
	print (colorize(Color.BLUE,'getting data..',bold=True))
	db = get_data()
	print (colorize(Color.BLUE,'\t-> done',bold=True))

	# open the output h5 file:
	out_db = h5py.File(OUT_FILE,'w')
	out_db.create_group('/data')
	print (colorize(Color.GREEN,'Storing the output in: '+OUT_FILE, bold=True))

	# get the names of the image files in the dataset:
	imnames = sorted(db['image'].keys())
	N = len(imnames)

	print('number of images: ', N)

	# initialize dictionary used to save to .mat file
	dict_dot_mat = {}
	dict_dot_mat['imnames'] = np.zeros((1,N*INSTANCE_PER_IMAGE), dtype=object)
	dict_dot_mat['wordBB'] = np.zeros((1,N*INSTANCE_PER_IMAGE), dtype=object)
	dict_dot_mat['charBB'] = np.zeros((1,N*INSTANCE_PER_IMAGE), dtype=object)
	dict_dot_mat['txt'] = np.zeros((1,N*INSTANCE_PER_IMAGE), dtype=object)

	global NUM_IMG
	if NUM_IMG < 0:
		NUM_IMG = N
	start_idx,end_idx = 0,min(NUM_IMG, N)

	RV3 = RendererV3(DATA_PATH, max_time=SECS_PER_IMG)
	image_count = 0
	for i in range(start_idx,end_idx):
		imname = imnames[i]
		try:
			# get the image:
			img = Image.fromarray(db['image'][imname][:])
			# get the pre-computed depth:
			#  there are 2 estimates of depth (represented as 2 "channels")
			#  here we are using the second one (in some cases it might be
			#  useful to use the other one):
			depth = db['depth'][imname][:].T
			depth = depth[:,:,1]
			# get segmentation:
			seg = db['seg'][imname][:].astype('float32')
			area = db['seg'][imname].attrs['area']
			label = db['seg'][imname].attrs['label']

			# re-size uniformly:
			sz = depth.shape[:2][::-1]
			img = np.array(img.resize(sz,Image.ANTIALIAS))
			seg = np.array(Image.fromarray(seg).resize(sz,Image.NEAREST))

			print (colorize(Color.RED,'%d of %d'%(i,end_idx-1), bold=True))
			res = RV3.render_text(imname,img,depth,seg,area,label,
									ninstance=INSTANCE_PER_IMAGE,viz=viz)

			if len(res) > 0:
				# non-empty : successful in placing text:
				# add_res_to_db(imname,res,out_db)
				for i in range(0, len(res)):
					# add to matrix dicts
					dict_dot_mat['imnames'][0][image_count] = res[i]['imnames']
					dict_dot_mat['wordBB'][0][image_count] = np.array(res[i]['wordBB'])
					dict_dot_mat['charBB'][0][image_count] = np.array(res[i]['charBB'])
					dict_dot_mat['txt'][0][image_count] = np.array(res[i]['txt'])

					if image_count != 0 and image_count % SAVE_MAT_INTERVAL == 0:
						save_file_mat(dict_dot_mat.copy(), image_count)

					image_count = image_count + 1
			# visualize the output:
			# save_res_to_imgs(imname, res)
			if viz:
				if 'q' in input(colorize(Color.RED,'continue? (enter to continue, q to exit): ',True)):
					break
		except:
			traceback.print_exc()
			print (colorize(Color.GREEN,'>>>> CONTINUING....', bold=True))
			continue

	print('before truncate: ', dict_dot_mat['imnames'])
	save_file_mat(dict_dot_mat.copy(), image_count)
	db.close()
	out_db.close()
	print('TIME ELAPSED: ', time.strftime('%H:%M:%S', time.gmtime(time.time() - start_time)))

if __name__=='__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Genereate Synthetic Scene-Text Images')
	parser.add_argument('--viz',action='store_true',dest='viz',default=False,help='flag for turning on visualizations')
	args = parser.parse_args()
	main(args.viz)
